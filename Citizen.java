import java.util.ArrayList;

public class Citizen {
	//Se agregan los atributos pedidos por el proyecto
	private Comunity comunidad;
	private int id;
	private Disease enfermedad;
	private boolean estado;
	//constructor que recibe el id del ciudadano y la comunidad en la que se encuentra
	Citizen(int id, Comunity com){
		this.id = id;
		this.comunidad = com;
	//Reconsiderar, se reconsideró y se puede realizar de otras formas, pero me quedé con esta por temas de tiempo, de requerirlo lo puedo cambiar.
		this.enfermedad = new Disease(this.comunidad.getEnfermedad().getProbabilidadInfeccion(), this.comunidad.getEnfermedad().getPromedioPasos());
		this.estado = false;
	}
	//este metodo actualiza el estado del cuidadano dependiendo del paso que se encuentra de iniciado su contagio
	//retorna el estado actualizado
	public boolean isEstado() {
		if(this.enfermedad.getContador() == this.enfermedad.getPromedioPasos()) {
			this.setEstado(true);
			this.setEnfermo(false);
		}
		return estado;
	}
	//setter de estado
	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	//getter del id
	public int getId() {
		return id;
	}
	//getter de la enfermedad
	public boolean isEnfermo() {
		return this.enfermedad.getEnfermo();
	}
	//setter de la enfermedad
	public void setEnfermo(boolean enfermo) {
		this.enfermedad.setEnfermo(enfermo);
	}
	//setter del contador, se comunica con la enfermedad de la persona
	public void setContador(int contador) {
		this.enfermedad.setContador(contador);
	}
	//metodo que analiza el estado según el step
	public void tratamiento() {
		if(this.isEnfermo()) {
		this.enfermedad.setContador(1);
		}
	}
	//metodo que verifica si la persona infecta a los contactos posibles según las probabilidades dadas
	public void contagio(ArrayList<Citizen> comunidad) {
		
			int contactos = this.comunidad.getPromedioConeccionesFisicas();
			int[] ciudadanosAleatorios = this.comunidad.ciudadanosAleatorios(comunidad, contactos);
			for(int i = 0; i < contactos; i++) {
				if(comunidad.get(ciudadanosAleatorios[i]).isEstado() == false) {
				if(this.comunidad.coneccionFisica() && this.enfermedad.infectado()) {
					//Cambiar orden (Cambiado)
					comunidad.get(ciudadanosAleatorios[i]).setEnfermo(true);
				}
			}
		}
	}	
}
