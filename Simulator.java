import java.util.ArrayList;
//la presente clase integra la funcionalidad de las demás para llevar a cabo la simulación que se asemeja el modelo "SIR"
public class Simulator {

    private ArrayList < Citizen > comunidad = new ArrayList < Citizen > ();
    private Comunity com;

    //se crea la comunidad en base a los datos comunitarios ingresados a Comunity en main

    public void creaComunidad(Comunity com) {
        this.com = com;

        for (int i = 0; i < this.com.getNumeroCiudadanos(); i++) {
            this.comunidad.add(new Citizen(i, com));
        }
        int[] ciudadanosAleatorios = com.ciudadanosAleatorios(this.comunidad, com.getNumeroInfectados()); 
        for(int i = 0; i < this.com.getNumeroInfectados(); i++) {
            this.comunidad.get(ciudadanosAleatorios[i]).setEnfermo(true);
        }
    }
    //El metodo run genera los pasos de manera ciclica
    public void run(int runs) {
        //en una versión posterior se podrían migrar los estados de resultado de enfermos, recuperados y sanos a Comunity
        //sin embargo como se realizó el proceso tomando la misma intancia de comunidad no habría diferencias en el proceso
    	int maximoInfectados[] = {0, 0};
    	for(int i = 0; i < runs; i++) {
        ArrayList < Citizen > enfermos =  new ArrayList < Citizen > ();
        ArrayList < Citizen > recuperadosMuertos = new ArrayList < Citizen > ();
        ArrayList < Citizen > sanos = new ArrayList < Citizen > ();
	    for(Citizen ciudadano: comunidad) {
	    	if(ciudadano.isEnfermo()) {
	    		enfermos.add(ciudadano);
	    	}
	    	else if(ciudadano.isEstado()) {
	    		recuperadosMuertos.add(ciudadano);
	    	}
	    	else {
	    		sanos.add(ciudadano);
	    	}
	    }
	    if(maximoInfectados[0] < enfermos.size()) {
	    	maximoInfectados[0] = enfermos.size();
	    	maximoInfectados[1] = i;
	    }
        com.setNumeroInfectados(enfermos.size());
        //se generan las salidas que muestran al usuario el estado de la pandemia según paso
        System.out.print("En el ciclo: " + i + " existen: " + this.com.getNumeroInfectados());
        System.out.print(" el nmero de recuperados es: " + recuperadosMuertos.size());
        System.out.println(" la cantidad de personas sin contagiarse es de: " + sanos.size());
        for(Citizen ciudadano: enfermos) {
        	ciudadano.tratamiento();
        	ciudadano.isEstado();
        	ciudadano.contagio(this.comunidad);
        	}
    	}
        //Finalmente se entregan datos resumen.
    	System.out.println("El pick de contagios fue de: " + maximoInfectados[0] + " En el ciclo: " + maximoInfectados[1]);
    }
}
