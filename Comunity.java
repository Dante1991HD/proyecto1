import java.util.ArrayList;
import java.util.Random;
//se importa random para las probabilidades de contacto, cantidad de contactos y de contagio
public class Comunity {
	//se agregan atributos según requerimientos...
	private int numeroCiudadanos;
	private int promedioConeccionesFisicas;
	private Disease enfermedad;
	private int numeroInfectados;
	private double probabilidadConeccionesFisicas;
	//constructor llamado en main, se pasa la misma comunidad en todo el programa
	Comunity(int numeroCiudadanos, int promedioConeccionesFisicas, Disease enfermedad, int numeroInfectados, double probabilidadConeccionesFisicas){
		this.numeroCiudadanos = numeroCiudadanos;
		this.promedioConeccionesFisicas = promedioConeccionesFisicas;
		this.enfermedad = enfermedad;
		this.numeroInfectados = numeroInfectados;
		this.probabilidadConeccionesFisicas = probabilidadConeccionesFisicas;
	}
	Comunity(){}
	//setters y getters que se llaman al contagiar y verificar estado de la comunidad
	public int getNumeroCiudadanos() {
		return numeroCiudadanos;
	}
	//los getters con randoms retornan true o false segun la probabilidad propuesta
	public int getPromedioConeccionesFisicas() {
		return new Random().nextInt(this.promedioConeccionesFisicas);
	}
	public boolean coneccionFisica() {
		
		return new Random().nextDouble() <= this.probabilidadConeccionesFisicas;
	}
	public Disease getEnfermedad() {
		return enfermedad;
	}
	public int getNumeroInfectados() {
		return numeroInfectados;
	}
	public void setNumeroInfectados(int numeroInfectados) {
		this.numeroInfectados = numeroInfectados;
	}
	//este metodo retorna cuidadanos aleatorios en creacion de comunidad o al contagiar.
	public int[] ciudadanosAleatorios(ArrayList<Citizen> comunidad, int cantidadCiudadanos) {
	int[] idUnico = new int[cantidadCiudadanos];
    for(int i =0; i<cantidadCiudadanos; i++) {
	    Citizen ciudadanoAleatorio = comunidad.get(new Random().nextInt(comunidad.size()));
	    idUnico[i] = ciudadanoAleatorio.getId();
    }
    	return idUnico;
	}
}
