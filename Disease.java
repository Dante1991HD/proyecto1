import java.util.Random;
// la case Disease es referencial con los atributos especificos de la enfermedad en cuestion,
// esto incluyendo true o false para la probabilidad de infectarse
public class Disease {
	//atributos según proyecto1
	private double probabilidadInfeccion;
	private int promedioPasos;
	private boolean enfermo;
	private int contador;
	//constructor con atributos por defecto como contador y enfermo = false
	Disease(double probabilidadInfeccion, int promedioPasos){
		this.probabilidadInfeccion = probabilidadInfeccion;
		this.promedioPasos = promedioPasos;
		this.enfermo = false;
		this.contador = 0;
	}
	//setters y getters de los atributos
	public boolean getEnfermo() {
		return enfermo;
	}
	public void setEnfermo(boolean enfermo) {
		this.enfermo = enfermo;
	}
	public int getContador() {
		return contador;
	}
	public void setContador(int contador) {
		this.contador += contador;
	}
	public int getPromedioPasos() {
		return promedioPasos;
	}
	public void setPromedioPasos(int promedioPasos) {
		this.promedioPasos = promedioPasos;
	}
	public double getProbabilidadInfeccion() {
		return this.probabilidadInfeccion;
	}
	//retorna true o false según probabilidad de infección.
	public boolean infectado() {
		return new Random().nextFloat() <= this.probabilidadInfeccion;
	}
}
