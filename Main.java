public class Main {
	//main crea la enfermedad, la comunidad y el simulador que los contendrá para realizar la simulación
	public static void main(String[] args) {
		//los valores para los objetos fueron tomados de manera unilateral, se pueden cambiar lógicamente.
		Disease enfermedad = new Disease(0.3, 2);
		Comunity com = new Comunity(20000, 8, enfermedad, 10, 0.9);
		Simulator sim = new Simulator();
		sim.creaComunidad(com);
		sim.run(45);
		
	}
}
